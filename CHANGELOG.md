# ${project-name} Change Log

All notable changes to this project will be documented in this file.

## [1.0.0]
### Added
- Now creates a folder for log if needed

## 0.1.0
- Beta Release

