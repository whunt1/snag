import context
import unittest
from snag import *

class BasicTests(unittest.TestCase):
    """Basic Test Cases"""
    
    def test_truncate(self):
        test_logger = logger.Logger(False, True, True, 0, "log")
        assert len(test_logger.truncate("12345", 0)) == 5
        assert len(test_logger.truncate("abcdefghijklmnopqrstuvwxyz", 20)) == 20
        assert len(test_logger.truncate("testtestesttest", 0)) == len("testtestesttest")

if __name__ == "__main__":
    unittest.main()
