example_config = [
    ["Adium","https://adium.im",".downloadInfo:a:*href","dmg","mac"],
    ["CCleaner","https://www.ccleaner.com/ccleaner/download?mac","#GTM__link--CC-mac-download-footer:*href","dmg","mac"],
    ["DiskInventoryX","http://derlien.com/downloads/index.html","#osversion:^:a:*href","dmg","mac"],
    ["EtreCheckPro","https://www.etrecheck.com/details","@Download EtreCheckPro directly:*href","app","mac"],
    ["MalwareBytes","https://www.malwarebytes.com/mac-download","#cta-mac-download-download-mbmac-en:*href","pkg","mac"],
    ["Perian","https://www.perian.org",".download_wrapper:h3:a:*href","dmg","mac"],
    ["Mozilla_Firefox","https://mozilla.org/en-US/firefox/all","-","dmg","mac"],
    ["Google_Chrome","https://dl.google.com/chrome/mac/stable/CHFA/googlechrome.dmg","+","dmg","mac"],
    ["Adware_Removal_Tool_by_TSA","https://www.techsupportall.com/download-art","+","exe","windows"],
    ["Super_Anti_Spyware","http://cdn.superantispyware.com/SUPERAntiSpyware.exe","+","exe","windows"],
    ["HitmanPro","https://www.hitmanpro.com/en-us/downloads.aspx","-","exe","windows"],
    ["HitmanPro.Alert","https://www.hitmanpro.com/en-us/downloads.aspx","-","exe","windows"]
]
